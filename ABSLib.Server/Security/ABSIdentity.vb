﻿Imports System
Imports System.Data.SqlClient
Imports Csla.Data
Imports Singular.Security
Imports Singular.Web.Security

Namespace ABSLib.Security

  ''' <summary>
  ''' ResetState Enumeration
  ''' </summary>
  Public Enum ResetState
    ''' <summary>
    ''' Normal state (No reset needed)
    ''' </summary>
    Normal = 0

    ''' <summary>
    ''' Indicates that the password must be reset
    ''' </summary>
    MustResetPassword = 1
  End Enum

  Public Class ABSIdentity
    Inherits WebIdentity(Of ABSIdentity)

#Region "Fields"
    Private firstName As String

    Private lastName As String

    Private emailAddress As String

    ''' <summary>
    ''' Is this the identity's first time signing in?
    ''' </summary>
    Private firstTimeLogin As Boolean

    Private resetState As ResetState
#End Region

#Region "Properties"
    Public ReadOnly Property EmailAddresss As String
      Get
        Return Me.emailAddress
      End Get
    End Property

    Public ReadOnly Property FirstNames As String
      Get
        Return Me.firstName
      End Get
    End Property

    Public ReadOnly Property LastNames As String
      Get
        Return Me.lastName
      End Get
    End Property

    ''' <summary>
    ''' Get the first time login indicator
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property FirstTimeLogins As Boolean
      Get
        Return Me.firstTimeLogin
      End Get
    End Property

    ''' <summary>
    ''' Get the password reset state
    ''' </summary>
    ''' <returns></returns>
    Public ReadOnly Property ResetStates As ResetState
      Get
        Return Me.resetState
      End Get
    End Property
#End Region

#Region "Methods"
    ''' <summary>
    ''' Marks the user as not first time login
    ''' </summary>
    Public Sub MarkNonFirstTimeLogin()
      Me.firstTimeLogin = False
      'Call a cmd proc to update this in the database
    End Sub

    ''' <summary>
    ''' After changing a user's password, call this to clear the reset state
    ''' </summary>
    Public Sub ChangedPassword()
      Me.resetState = ResetState.Normal
    End Sub

    ''' <summary>
    ''' Setup a SQL command to do a login check
    ''' </summary>
    ''' <param name="cmd"></param>
    ''' <param name="Criteria"></param>
    Protected Overrides Sub SetupSqlCommand(ByVal cmd As SqlCommand, ByVal Criteria As IdentityCriterea)
      MyBase.SetupSqlCommand(cmd, Criteria)
      cmd.CommandText = "CmdProcs.WebLogin"
      cmd.Parameters("@Password").Value =
        ABSWebSecurity.GetPasswordHash(cmd.Parameters("@Password").Value.ToString())
    End Sub

    Protected Overrides Sub ReadExtraProperties(ByVal sdr As SafeDataReader, ByRef StartIndex As Integer)
      MyBase.ReadExtraProperties(sdr, StartIndex)
      Me.emailAddress = sdr.GetString(StartIndex)
      Me.firstTimeLogin = sdr.GetBoolean(StartIndex + 1)
      Me.resetState = CType(sdr.GetInt32(StartIndex + 2), ResetState)
    End Sub
#End Region

  End Class
End Namespace
