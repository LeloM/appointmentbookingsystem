﻿Imports System
Imports Singular
Imports Singular.Web.Security

Namespace ABSLib.Security

  <Serializable()>
  Public Class ABSWebSecurity
    Inherits WebSecurity(Of WebPrinciple(Of ABSIdentity), ABSIdentity)

    ''' <summary>
    ''' Generates a has for a user password
    ''' </summary>
    ''' <param name="plainTextPassword">The password to hash</param>
    ''' <returns>A hashed password</returns>
    Public Shared Function GetPasswordHash(ByVal plainTextPassword As String) As String
      Return Encryption.GetStringHash(plainTextPassword, Encryption.HashType.Sha256)
    End Function
  End Class
End Namespace
