﻿Imports System
Imports ABSLib.ABSLib.Maintenance
Imports Singular.CommonData

Namespace ABSLib.ABSLib
  Public Class CommonData
    Inherits CommonDataBase(Of ABSLib.CommonData.ABSCachedLists)

    <Serializable()>
    Public Class ABSCachedLists
      Inherits CommonDataBase(Of ABSCachedLists).CachedLists

    End Class

  End Class

End Namespace
