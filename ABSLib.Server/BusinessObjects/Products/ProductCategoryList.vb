﻿' Generated 01 Sep 2021 16:12 - Singular Systems Object Generator Version 2.2.694
'<auto-generated/>
Imports Csla
Imports Csla.Serialization
Imports Csla.Data
Imports System.ComponentModel
Imports System.ComponentModel.DataAnnotations
Imports Singular
Imports Singular.Misc
Imports Singular.DataAnnotations
Imports System.Data.SqlClient


Namespace ABSLib.Products

  <Serializable()>
  Public Class ProductCategoryList
    Inherits SingularBusinessListBase(Of ProductCategoryList, ProductCategory)

#Region " Business Methods "

    Public Function GetItem(ProductCategoryID As Integer) As ProductCategory

      For Each child As ProductCategory In Me
        If child.ProductCategoryID = ProductCategoryID Then
          Return child
        End If
      Next
      Return Nothing

    End Function

    Public Overrides Function ToString() As String

      Return "Product Categorys"

    End Function

#End Region

#Region " Data Access "

    <Serializable()>
    Public Class Criteria
      Inherits CriteriaBase(Of Criteria)

      Public Sub New()


      End Sub

    End Class

    Public Shared Function NewProductCategoryList() As ProductCategoryList

      Return New ProductCategoryList()

    End Function

    Public Sub New()

      ' must have parameter-less constructor

    End Sub

    Public Shared Function GetProductCategoryList() As ProductCategoryList

      Return DataPortal.Fetch(Of ProductCategoryList)(New Criteria())

    End Function

    Protected Sub Fetch(sdr As SafeDataReader)

      Me.RaiseListChangedEvents = False
      While sdr.Read
        Me.Add(ProductCategory.GetProductCategory(sdr))
      End While
      Me.RaiseListChangedEvents = True

    End Sub

    Protected Overrides Sub DataPortal_Fetch(criteria As Object)

      Dim crit As Criteria = criteria
      Using cn As New SqlConnection(Singular.Settings.ConnectionString)
        cn.Open()
        Try
          Using cm As SqlCommand = cn.CreateCommand
            cm.CommandType = CommandType.StoredProcedure
            cm.CommandText = "GetProcs.getProductCategoryList"
            Using sdr As New SafeDataReader(cm.ExecuteReader)
              Fetch(sdr)
            End Using
          End Using
        Finally
          cn.Close()
        End Try
      End Using

    End Sub

#End Region

  End Class

End Namespace