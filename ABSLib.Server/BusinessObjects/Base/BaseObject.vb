﻿Imports System
Imports Singular

Namespace ABSLib
  <Serializable()>
  Public Class ABSBusinessBase(Of C As ABSBusinessBase(Of C))
    Inherits SingularBusinessBase(Of C)
    'Base class for business objects
  End Class

  <Serializable()>
  Public Class ABSBusinessListBase(Of T As ABSBusinessListBase(Of T, C), C As ABSBusinessBase(Of C))
    Inherits SingularBusinessListBase(Of T, C)
    'Base class for business object lists
  End Class

  <Serializable()>
  Public Class ABSReadOnlyBase(Of C As ABSReadOnlyBase(Of C))
    Inherits SingularReadOnlyBase(Of C)
    'Base class for read only business objects
  End Class

  <Serializable()>
  Public Class ABSReadOnlyListBase(Of T As ABSReadOnlyListBase(Of T, C), C As ABSReadOnlyBase(Of C))
    Inherits SingularReadOnlyListBase(Of T, C)
    'Base class for read only business object lists
  End Class

End Namespace
