﻿Namespace CustomControls.Inspinia
  Public NotInheritable Class InspiniaConstants
    Public NotInheritable Class CSSClasses
      Public Const CollapseLink As String = "collapse-link"
      Public Const Dropzone As String = "dropzone"
      Public Const Fallback As String = "fallback"
      Public Const FormEditor As String = "form-control"
      Public Const FormGroup As String = "form-group"
      Public Const FormLabel As String = "control-label"
      Public Const IBox As String = "ibox"
      Public Const IBoxContent As String = "ibox-content"
      Public Const IBoxTitle As String = "ibox-title"
      Public Const IBoxTools As String = "ibox-tools"
      Public Const ICheckID As String = "i-check"
      Public Const Label As String = "label"
    End Class
  End Class
End Namespace
