﻿Imports Singular.Web.Controls.HelperControls

Namespace CustomControls.Inspinia
  ''' <summary>
  ''' Inspinia Accessors Helper - Used to style page with <see href="http://webapplayers.com/inspinia_admin-v2.8/index.html">Inspinia</see>
  ''' </summary>
  ''' <typeparam name="ObjectType">The Object Type</typeparam>
  Public Class InspiniaAccessors(Of ObjectType)

    Private ReadOnly parent As HelperAccessors(Of ObjectType)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="InspiniaAccessors"/> class.
    ''' </summary>
    ''' <param name="parent">Instance of the parent Helper Accessors class</param>
    Public Sub New(parent As HelperAccessors(Of ObjectType))
      Me.parent = parent
    End Sub

    ''' <summary>
    ''' Renders a Inspinia I-Box Control
    ''' </summary>
    ''' <param name="titleText">Title to display in a h5 element with-in the I-Box-Title div</param>
    ''' <param name="fontAwesomeIcon">Optional parameter used to display FontAwesome Icon in title</param>
    ''' <param name="fontAwesomeStyles">Optional parameter to add additional CSS classes to the font-awesome icon 'i' element</param>
    ''' <param name="labelTitle">Optional parameter to add display the label Title</param>
    ''' <param name="labelTitleClass">Optional parameter to add additional CSS classes to the label title text span element</param>
    ''' <returns>Inspinia <see cref="IBox"/> Control</returns>
    Public Function IBox(titleText As String,
                        Optional fontAwesomeIcon As FontAwesomeIcon = FontAwesomeIcon.None,
                        Optional fontAwesomeStyles As String = "",
                        Optional labelTitle As String = "",
                        Optional labelTitleClass As String = "") As IBox(Of ObjectType)

      Dim iBoxCriteria = New IBoxCriteria With {
        .Title = titleText,
        .LabelTitle = labelTitle,
        .LabelTitleClass = labelTitleClass,
        .FontAwesomeIcon = fontAwesomeIcon,
        .FontAwesomeClass = fontAwesomeStyles
      }

      Dim mIBox As New IBox(Of ObjectType)(iBoxCriteria)
      Return Me.parent.SetupControl(mIBox)
    End Function

    ''' <summary>
    ''' Renders a Inspinia Form Group Control - Grouping Label and Editor for
    ''' </summary>
    ''' <param name="editor">Object being edited</param>
    ''' <returns>Inspinia <see cref="FormGroup"/> Control</returns>
    Public Function FormGroup(editor As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object))) As FormGroup(Of ObjectType)
      Dim mFormGroup As New FormGroup(Of ObjectType)(editor)
      Return Me.parent.SetupControl(mFormGroup)
    End Function

    ''' <summary>
    ''' Renders a Inspinia Form Group Control with CSS class for form group div - Grouping Label and Editor for
    ''' </summary>
    ''' <param name="editor">Object being edited - similar to editor for with label</param>
    ''' <param name="cssClasses">CSS Class for form group Div</param>
    ''' <returns>Inspinia <see cref="FormGroup"/>  Control</returns>
    Public Function FormGroup(editor As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object)),
                              cssClasses As String) As FormGroup(Of ObjectType)
      Dim mFormGroup As New FormGroup(Of ObjectType)(editor)
      mFormGroup.AddClass(cssClasses)
      Return Me.parent.SetupControl(mFormGroup)
    End Function

    ''' <summary>
    ''' Renders a Inspinia I-CheckBox Control
    ''' </summary>
    ''' <param name="editor">Object being edited</param>
    ''' <param name="cssClasses">CSS Class for I-CheckBox Div</param>
    ''' <param name="renderLabel">Allow caller to display I-CheckBox label</param>
    ''' <param name="labelText">Allow caller to specify label otherwise property info name will be used if available</param>
    ''' <returns>Inspinia <see cref="ICheckBox"/> Control</returns>
    Public Function ICheckBox(editor As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object)),
                              Optional cssClasses As String = "",
                              Optional renderLabel As Boolean = True,
                              Optional labelText As String = "") As ICheckBox(Of ObjectType)
      Dim mICheckBox As New ICheckBox(Of ObjectType)(editor, renderLabel, labelText)
      If Not String.IsNullOrEmpty(cssClasses) Then
        mICheckBox.AddClass(cssClasses)
      End If
      Return Me.parent.SetupControl(mICheckBox, editor)
    End Function

    ''' <summary>
    ''' Renders a Inspinia I-Dropzone Control
    ''' </summary>
    ''' <returns>Inspinia <see cref="IDropzone"/> Control</returns>
    Public Function IDropzone() As IDropzone(Of ObjectType)

      Dim iDropzoneCriteria = New IDropzoneCriteria()
      Dim mIDropzone As New IDropzone(Of ObjectType)(iDropzoneCriteria)

      Return Me.parent.SetupControl(mIDropzone)
    End Function

    ''' <summary>
    ''' Renders a Inspinia I-Dropzone Control
    ''' </summary>
    ''' <param name="iDropzoneCriteria"><see cref="IDropzoneCriteria" /> allow caller to overwrite default values</param>
    ''' <returns>Inspinia <see cref="IDropzone"/> Control</returns>
    Public Function IDropzone(iDropzoneCriteria As IDropzoneCriteria) As IDropzone(Of ObjectType)
      Dim mIDropzone As New IDropzone(Of ObjectType)(iDropzoneCriteria)

      Return Me.parent.SetupControl(mIDropzone)
    End Function
  End Class
End Namespace
