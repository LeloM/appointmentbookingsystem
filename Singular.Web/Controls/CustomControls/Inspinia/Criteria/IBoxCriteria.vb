﻿Namespace CustomControls.Inspinia
  Public Class IBoxCriteria

    ''' <summary>
    ''' Gets or sets the Font Awesome Icon in the I-Box
    ''' </summary>
    ''' <returns>Font Awesome Icon</returns>
    Public Property FontAwesomeIcon As FontAwesomeIcon

    ''' <summary>
    ''' Gets or sets Font Awesome Class allow developer to add additional classes to the Font Awesome Class
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property FontAwesomeClass As String

    ''' <summary>
    ''' Gets or sets I-Box Title Description
    ''' </summary>
    ''' <returns>Title string value</returns>
    Public Property Title As String

    ''' <summary>
    ''' Gets or sets I-Box Label Title Description
    ''' </summary>
    ''' <returns>Label Title string value</returns>
    Public Property LabelTitle As String

    ''' <summary>
    ''' Gets or sets I-Box Label Title Class Description
    ''' </summary>
    ''' <returns>Label Title Class string value</returns>
    Public Property LabelTitleClass As String

  End Class
End Namespace