﻿Namespace CustomControls.Inspinia
  Public Class IDropzoneCriteria

    ''' <summary>
    ''' Gets or sets the dropzone color
    ''' </summary>
    ''' <returns>Font Awesome Icon</returns>
    Public Property DropzoneID As String = "file-dropzone"

    ''' <summary>
    ''' Gets or sets the dropzone color
    ''' </summary>
    ''' <returns>Font Awesome Icon</returns>
    Public Property DropzoneColor As String = "#5A8F94"

    ''' <summary>
    ''' Gets or sets dropzone mouse pointer
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzoneMousePointer As String = "pointer"

    ''' <summary>
    ''' Gets or sets dropzone span width
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzoneSpanWidth As String = "25px"

    ''' <summary>
    ''' Gets or sets dropzone span height
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzoneDropFilesSpanText As String = "Drop files to upload, or "

    ''' <summary>
    ''' Gets or sets dropzone span height
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzoneDropFilesSpanPaddingBottom As String = "10px"

    ''' <summary>
    ''' Gets or sets dropzone span height
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzoneSpanHeight As String = "20px"

    ''' <summary>
    ''' Gets or sets dropzone span height
    ''' </summary>
    ''' <returns>FontAwesome Class string value</returns>
    Public Property DropzonerowseFilesLinkText As String = "browse"

    ''' <summary>
    ''' Gets or sets dropzone font size
    ''' </summary>
    ''' <returns>Title string value</returns>
    Public Property DropzoneFontSize As String = "24"

  End Class
End Namespace