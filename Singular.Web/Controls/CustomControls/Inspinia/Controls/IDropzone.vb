﻿Imports Singular.Web.Controls.HelperControls

Namespace CustomControls.Inspinia
  Public Class IDropzone(Of ObjectType)
    Inherits HTMLDiv(Of ObjectType)

    Private Const StyleCursor As String = "cursor"
    Private Const StyleColor As String = "color"
    Private ReadOnly mIDropzoneCriteria As IDropzoneCriteria
    Private mDropzoneInnerDiv As HTMLDiv(Of ObjectType)
    Private mDropzoneSpan As HTMLTag(Of ObjectType)
    Private mDropzoneDropFilesSpan As HTMLTag(Of ObjectType)
    Private mDropzoneBrowseLink As Link(Of ObjectType)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="IDropzone"/> class.
    ''' </summary>
    ''' <param name="IDropzoneCriteria">An instance of the <see cref="IDropzoneCriteria"/> class, used to set values with in I-Dropzone control</param>
    Public Sub New(iDropzoneCriteria As IDropzoneCriteria)
      Me.mIDropzoneCriteria = iDropzoneCriteria
    End Sub

    Protected Friend Overrides Sub Setup()
      MyBase.Setup()

      Me.AddClass(InspiniaConstants.CSSClasses.Dropzone)
      Me.ID = mIDropzoneCriteria.DropzoneID
      Me.Style(StyleCursor) = mIDropzoneCriteria.DropzoneMousePointer
      Me.Style(StyleColor) = mIDropzoneCriteria.DropzoneColor

      mDropzoneInnerDiv = Me.Helpers.Div()

      mDropzoneSpan = mDropzoneInnerDiv.Helpers.Span()
      mDropzoneSpan.Style.Width = mIDropzoneCriteria.DropzoneSpanWidth
      mDropzoneSpan.Style.Height = mIDropzoneCriteria.DropzoneSpanHeight
      mDropzoneSpan.Style.Display = Singular.Web.Display.inlineblock

      mDropzoneDropFilesSpan = mDropzoneInnerDiv.Helpers.Span(mIDropzoneCriteria.DropzoneDropFilesSpanText)
      mDropzoneDropFilesSpan.Style.FontSize = mIDropzoneCriteria.DropzoneFontSize
      mDropzoneDropFilesSpan.Style.Padding(bottom:=mIDropzoneCriteria.DropzoneDropFilesSpanPaddingBottom)

      mDropzoneBrowseLink = mDropzoneInnerDiv.Helpers.LinkFor(LinkTextString:=mIDropzoneCriteria.DropzonerowseFilesLinkText)
      mDropzoneBrowseLink.Style.FontSize = mIDropzoneCriteria.DropzoneFontSize
    End Sub

    ''' <summary>
    ''' Gets Dropzone Inner Div
    ''' </summary>
    ''' <returns><see cref="HTMLDiv"/></returns>
    Public ReadOnly Property DropzoneInnerDiv As HTMLDiv(Of ObjectType)
      Get
        Return mDropzoneInnerDiv
      End Get
    End Property

    ''' <summary>
    ''' Gets Drop zone Span
    ''' </summary>
    ''' <returns><see cref="HTMLTag"/></returns>
    Public ReadOnly Property DropzoneSpan As HTMLTag(Of ObjectType)
      Get
        Return mDropzoneSpan
      End Get
    End Property

    ''' <summary>
    ''' Gets Drop zone Span
    ''' </summary>
    ''' <returns><see cref="HTMLTag"/></returns>
    Public ReadOnly Property DropzoneDropFilesSpan As HTMLTag(Of ObjectType)
      Get
        Return mDropzoneDropFilesSpan
      End Get
    End Property

    ''' <summary>
    ''' Gets Drop zone Span
    ''' </summary>
    ''' <returns><see cref="HTMLTag"/></returns>
    Public ReadOnly Property DropzoneBrowseLink As Link(Of ObjectType)
      Get
        Return mDropzoneBrowseLink
      End Get
    End Property

  End Class
End Namespace
