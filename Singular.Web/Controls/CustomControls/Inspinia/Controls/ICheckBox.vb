﻿Imports System.Web.UI.WebControls.Expressions

Namespace CustomControls.Inspinia

  ''' <summary>
  ''' Inspinia Check Box
  ''' </summary>
  ''' <typeparam name="ObjectType">The Object Type</typeparam>
  Public Class ICheckBox(Of ObjectType)
    Inherits HTMLDiv(Of ObjectType)

    Private Const SUIRuleBorderClass As String = "SUI-RuleBorder"
    Private ReadOnly mboundProperty As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object))
    Private ReadOnly mRenderLabel As Boolean
    Private mICheckboxLabel As HTMLTag(Of ObjectType)
    Private mICheckboxInput As EditorBase(Of ObjectType)
    Private mLabelText As String

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ICheckBox"/> class.
    ''' </summary>
    ''' <param name="boundProperty">Property being bound to I-Check-box</param>
    ''' <param name="renderLabel">Indicator to allow label to render without text</param>
    ''' <param name="labelText">Allow caller to specify label otherwise property info name will be used if available</param>
    Public Sub New(boundProperty As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object)),
                   Optional renderLabel As Boolean = True,
                   Optional labelText As String = "")
      Me.mboundProperty = boundProperty
      Me.mRenderLabel = renderLabel
      Me.mLabelText = labelText
    End Sub

    Protected Friend Overrides Sub Setup()
      MyBase.Setup()

      Me.ID = (InspiniaConstants.CSSClasses.ICheckID)
      Me.RenderCheckbox()
      Me.RenderLabel()
    End Sub
    Private Sub RenderCheckbox()
      mICheckboxInput = Helpers.EditorFor(mboundProperty)
      mICheckboxInput.AddClass(SUIRuleBorderClass)
    End Sub

    Private Sub RenderLabel()
      If mRenderLabel Then
        If String.IsNullOrEmpty(Me.mLabelText) AndAlso PropertyInfo IsNot Nothing Then
          Me.mLabelText = Singular.Reflection.GetDisplayName(PropertyInfo)
        End If
      Else
        Me.mLabelText = String.Empty
      End If

      mICheckboxLabel = Helpers.HTMLTag(HTMLTags.Label)
      'Add 'for' Binding to link the html label tag to its editor
      mICheckboxLabel.AddBinding(KnockoutBindingString.UID, GetForJS)
      Dim mLabelTextSpan = mICheckboxLabel.Helpers.Span(Me.mLabelText)
      mLabelTextSpan.AddClass("i-check-text")
    End Sub

    ''' <summary>
    ''' Gets I-Check-box input allowing caller to access I-Check-box input properties
    ''' </summary>
    ''' <returns>EditorBase for Form Group Editor</returns>
    Public ReadOnly Property ICheckboxInput As EditorBase(Of ObjectType)
      Get
        Return mICheckboxInput
      End Get
    End Property

    ''' <summary>
    ''' Gets I-Check-box label enabling the caller to access I-Check-box label properties
    ''' </summary>
    ''' <returns>HTMLDiv for I-Box-Div</returns>
    Public ReadOnly Property ICheckboxLabel As HTMLTag(Of ObjectType)
      Get
        Return mICheckboxLabel
      End Get
    End Property

  End Class
End Namespace
