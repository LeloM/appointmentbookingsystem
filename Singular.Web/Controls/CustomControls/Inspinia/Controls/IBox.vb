﻿Imports Singular.Web.Controls.HelperControls

Namespace CustomControls.Inspinia

  ''' <summary>
  ''' Inspinia IBox Helper
  ''' </summary>
  ''' <typeparam name="ObjectType">The Object Type</typeparam>
  Public Class IBox(Of ObjectType)
    Inherits HelperBase(Of ObjectType)

    Private ReadOnly mIBoxCriteria As IBoxCriteria
    Private mIBoxDiv As HTMLDiv(Of ObjectType)
    Private mIBoxTitleDiv As HTMLDiv(Of ObjectType)
    Private mIBoxToolsDiv As HTMLDiv(Of ObjectType)
    Private mIBoxContentDiv As HTMLDiv(Of ObjectType)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="IBox"/> class.
    ''' </summary>
    ''' <param name="iBoxCriteria">An instance of the <see cref="mIBoxCriteria"/> class, used to set values with in I-Box control</param>
    Public Sub New(iBoxCriteria As IBoxCriteria)
      Me.mIBoxCriteria = iBoxCriteria
    End Sub

    Protected Friend Overrides Sub Setup()
      MyBase.Setup()

      mIBoxDiv = RootHelpers.DivC(InspiniaConstants.CSSClasses.IBox)
      With mIBoxDiv
        Me.RenderIBoxTitle()
        mIBoxContentDiv = .Helpers.DivC(InspiniaConstants.CSSClasses.IBoxContent)
      End With
    End Sub

    Private Sub RenderIBoxTitle()
      mIBoxTitleDiv = mIBoxDiv.Helpers.DivC(InspiniaConstants.CSSClasses.IBoxTitle)
      With mIBoxTitleDiv
        If Not mIBoxCriteria.FontAwesomeIcon = FontAwesomeIcon.None Then
          .Helpers.HTMLTag(HTMLTags.I).AddClass($"{Misc.GetFontAwesomeClass(mIBoxCriteria.FontAwesomeIcon)} {mIBoxCriteria.FontAwesomeClass}")
        End If
        .Helpers.HTML().Heading5(mIBoxCriteria.Title)
        Me.RenderIBoxTools()
      End With
    End Sub

    Private Sub RenderIBoxTools()
      mIBoxToolsDiv = mIBoxTitleDiv.Helpers.DivC(InspiniaConstants.CSSClasses.IBoxTools)
      With mIBoxToolsDiv
        If Not String.IsNullOrEmpty(mIBoxCriteria.LabelTitle) Then
          Dim LabelTitle = .Helpers.HTMLTag(HTMLTags.Span)
          With LabelTitle
            .AddClass(InspiniaConstants.CSSClasses.Label)
            .AddClass(mIBoxCriteria.LabelTitleClass)
            .Helpers.HTML(mIBoxCriteria.LabelTitle)
          End With
        End If
        Dim ToolAnchor = .Helpers.HTMLTag(HTMLTags.Anchor)
        With ToolAnchor
          .AddClass(InspiniaConstants.CSSClasses.CollapseLink)
          .Helpers.HTMLTag(HTMLTags.I).AddClass(Misc.GetFontAwesomeClass(FontAwesomeIcon.chevron_up))
        End With
      End With
    End Sub

    ''' <summary>
    ''' Gets root helpers for I-box
    ''' </summary>
    ''' <returns>HelperAccessors</returns>
    Public ReadOnly Property RootHelpers As HelperAccessors(Of ObjectType)
      Get
        Return mHelpers
      End Get
    End Property

    ''' <summary>
    ''' Gets helpers for I-box-content
    ''' </summary>
    ''' <returns>Helper Accessors from I-box-content</returns>
    Public Shadows ReadOnly Property Helpers As HelperAccessors(Of ObjectType)
      Get
        Return mIBoxContentDiv.Helpers
      End Get
    End Property

    ''' <summary>
    ''' Gets I-Box-Div and allowing caller to access I-Box-Div properties
    ''' </summary>
    ''' <returns>HTMLDiv for I-Box-Div</returns>
    Public ReadOnly Property IBoxDiv As HTMLDiv(Of ObjectType)
      Get
        Return mIBoxDiv
      End Get
    End Property

    ''' <summary>
    ''' Gets I-Box-Div and allowing caller to access I-Box-Title-Div properties
    ''' </summary>
    ''' <returns>HTMLDiv for I-Box-Title-Div</returns>
    Public ReadOnly Property IBoxTitleDiv As HTMLDiv(Of ObjectType)
      Get
        Return mIBoxTitleDiv
      End Get
    End Property

    ''' <summary>
    ''' Gets I-Box-Div and allowing caller to access I-Box-Tools-Div properties
    ''' </summary>
    ''' <returns>HTMLDiv for I-Box-Tools-Div</returns>
    Public ReadOnly Property IBoxToolsDiv As HTMLDiv(Of ObjectType)
      Get
        Return mIBoxToolsDiv
      End Get
    End Property

    Protected Friend Overrides Sub Render()
      MyBase.Render()
      RenderChildren()
    End Sub
  End Class
End Namespace
