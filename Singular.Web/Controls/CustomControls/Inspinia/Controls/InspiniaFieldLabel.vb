﻿Imports Singular.Web.Controls.HelperControls

Namespace CustomControls.Inspinia

  ''' <summary>
  ''' Inspinia Field Label Helper
  ''' </summary>
  ''' <typeparam name="ObjectType"></typeparam>
  Public Class InspiniaFieldLabel(Of ObjectType)
    Inherits HelperBase(Of ObjectType)

    Private ReadOnly mRenderLabel As Boolean
    Public Property LabelText As String = ""

    ''' <summary>
    ''' Initializes a new instance of the <see cref="InspiniaFieldLabel"/> class.
    ''' </summary>
    Public Sub New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="InspiniaFieldLabel"/> class.
    ''' </summary>
    ''' <param name="renderLabel">Indicator to allow label to render without text</param>
    ''' <param name="labelText">Allow caller to specify label otherwise property info name will be used if available</param>
    Public Sub New(Optional renderLabel As Boolean = True,
                   Optional labelText As String = "")
      Me.LabelText = labelText
      Me.mRenderLabel = renderLabel
    End Sub

    Friend Overrides ReadOnly Property ControlSettingType As ControlSettingType
      Get
        Return ControlSettingType.Label
      End Get
    End Property

    Protected Friend Overrides Sub Setup()
      MyBase.Setup()
      If PropertyInfo IsNot Nothing Then
        'Add 'LabelFor' Binding to link this label to its Editor
        AddBinding(KnockoutBindingString.UID, GetForJS)
      End If
    End Sub

    Protected Friend Overrides Sub Render()
      MyBase.Render()
      With Writer
        WriteFullStartTag("label", TagType.Normal)
        If Me.mRenderLabel Then
          If String.IsNullOrEmpty(Me.LabelText) AndAlso PropertyInfo IsNot Nothing Then
            Me.LabelText = Singular.Reflection.GetDisplayName(PropertyInfo)
          End If
        End If
        .Write(Me.LabelText)
        .WriteEndTag("label")
      End With
    End Sub
  End Class
End Namespace
