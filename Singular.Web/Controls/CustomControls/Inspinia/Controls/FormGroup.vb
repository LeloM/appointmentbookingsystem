﻿Namespace CustomControls.Inspinia

  ''' <summary>
  ''' Inspinia IBox Helper
  ''' </summary>
  ''' <typeparam name="ObjectType">The Object Type</typeparam>
  Public Class FormGroup(Of ObjectType)
    Inherits HTMLDiv(Of ObjectType)

    Private ReadOnly mEditor As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object))
    Private mFormlabel As FieldLabel(Of ObjectType)
    Private mFormEditor As EditorBase(Of ObjectType)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="FormGroup"/> class.
    ''' </summary>
    ''' <param name="editor">Object being edited - similar to editor for</param>
    Public Sub New(editor As System.Linq.Expressions.Expression(Of System.Func(Of ObjectType, Object)))
      Me.mEditor = editor
    End Sub

    Protected Friend Overrides Sub Setup()
      MyBase.Setup()

      Me.AddClass(InspiniaConstants.CSSClasses.FormGroup)
      mFormlabel = Helpers.LabelFor(mEditor)
      mFormlabel.AddClass(InspiniaConstants.CSSClasses.FormLabel)

      mFormEditor = Helpers.EditorFor(mEditor)
      mFormEditor.AddClass(InspiniaConstants.CSSClasses.FormEditor)

    End Sub

    ''' <summary>
    ''' Gets Form Group Label
    ''' </summary>
    ''' <returns>FieldLabel for Form Group Label</returns>
    Public ReadOnly Property Editorlabel As FieldLabel(Of ObjectType)
      Get
        Return mFormlabel
      End Get
    End Property

    ''' <summary>
    ''' Gets Form Group Editor
    ''' </summary>
    ''' <returns>EditorBase for Form Group Editor</returns>
    Public ReadOnly Property Editor As EditorBase(Of ObjectType)
      Get
        Return mFormEditor
      End Get
    End Property
  End Class
End Namespace
