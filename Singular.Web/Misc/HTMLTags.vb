﻿Imports System.ComponentModel

''' <summary>
''' Enums for HTML Tags
''' For more info on HTML Tags go to <see href="https://www.w3schools.com/tags/tag_comment.asp">W3schools</see>
''' </summary>
Public Enum HTMLTags

  ''' <summary>
  ''' HTML a Tag.
  ''' The a tag defines a hyper-link, which is used to link from one page to another.
  ''' The most important attribute of the a element is the href attribute, which indicates the link's destination.
  ''' </summary>
  <Description("a")> Anchor

  ''' <summary>
  ''' HTML abbr Tag.
  ''' The abbr tag defines an abbreviation or an acronym, like "HTML", "CSS", "Mr.", "Dr.", "ASAP", "ATM".
  ''' Tip: Use the global title attribute to show the description for the abbreviation/acronym when you mouse over the element.
  ''' </summary>
  <Description("abbr")> Abbreviation

  ''' <summary>
  ''' HTML address Tags
  ''' The address tag defines the contact information for the author/owner of a document or an article.
  ''' The contact information can be an email address, URL, physical address, phone number, social media handle, etc.
  ''' The text in the address element usually renders in italic, and browsers will always add a line break before and after the address element.
  ''' </summary>
  <Description("address")> Address

  ''' <summary>
  ''' HTML area Tags
  ''' The area tag defines an area inside an image map (an image map is an image with clickable areas).
  ''' area elements are always nested inside a map tag.
  ''' Note: The usemap attribute in img is associated with the map element's name attribute, and creates a relationship between the image and the map.
  ''' </summary>
  <Description("area")> Area

  ''' <summary>
  ''' HTML article Tags
  ''' The article tag specifies independent, self-contained content.
  ''' An article should make sense on its own and it should be possible to distribute it independently from the rest of the site.
  ''' Note: The article element does not render as anything special in a browser. However, you can use CSS to style the article element (see example below).
  ''' </summary>
  <Description("article")> Article

  ''' <summary>
  ''' HTML aside Tags
  ''' The aside tag defines some content aside from the content it is placed in.
  ''' The aside content should be indirectly related to the surrounding content.
  ''' Tip: The aside content is often placed as a sidebar in a document.
  ''' Note: The aside element does not render as anything special in a browser. However, you can use CSS to style the aside element.
  ''' </summary>
  <Description("aside")> Aside

  ''' <summary>
  ''' HTML audio Tags
  ''' The audio tag is used to embed sound content in a document, such as music or other audio streams.
  ''' The audio tag contains one or more source tags with different audio sources. The browser will choose the first source it supports.
  ''' The text between the audio open and closing tags will only be displayed in browsers that do not support the audio element.
  ''' </summary>
  <Description("audio")> Audio

  ''' <summary>
  ''' HTML b Tags
  ''' The b tag specifies bold text without any extra importance.
  ''' </summary>
  <Description("b")> Bold

  ''' <summary>
  ''' HTML base Tags
  ''' The base tag specifies the base URL and/or target for all relative URLs in a document.
  ''' The base tag must have either an href or a target attribute present, or both.
  ''' There can only be one single base element in a document, and it must be inside the head element.
  ''' </summary>
  <Description("base")> Base

  ''' <summary>
  ''' HTML bdi Tags
  ''' BDI stands for Bi-Directional Isolation.
  ''' The bdi tag isolates a part of text that might be formatted in a different direction from other text outside it.
  ''' This element is useful when embedding user-generated content with an unknown text direction.
  ''' </summary>
  <Description("bdi")> BiDirectionalIsolation

  ''' <summary>
  ''' HTML bdi Tags
  ''' BDO stands for Bi-Directional Override.
  ''' The bdo tag is used to override the current text direction.
  ''' </summary>
  <Description("bdo")> BiDirectionalOverride

  ''' <summary>
  ''' HTML blockquote Tags
  ''' The blockquote tag specifies a section that is quoted from another source.
  ''' Browsers usually indent blockquote elements.
  ''' </summary>
  <Description("blockquote")> Blockquote

  ''' <summary>
  ''' HTML br Tags
  ''' The br tag inserts a single line break.
  ''' The br tag is useful for writing addresses.
  ''' The br tag is an empty tag which means that it has no end tag.
  ''' </summary>
  <Description("br")> Break

  ''' <summary>
  ''' HTML button  Tags
  ''' The button tag defines a clickable button.
  ''' Inside a button element you can put text (and tags like i, strong, br, img, etc.). This is not possible with a button created with the input element!
  ''' Tip: Always specify the type attribute for a butto> element, to tell browsers what type of button it is.
  ''' Tip: You can easily style buttons with CSS!
  ''' </summary>
  <Description("button")> Button

  ''' <summary>
  ''' HTML canvas Tags
  ''' The canvas tag is used to draw graphics, on the fly, via scripting (usually JavaScript).
  ''' The canvas tag is transparent, and is only a container for graphics, you must use a script to actually draw the graphics.
  ''' Any text inside the canvas element will be displayed in browsers with JavaScript disabled and in browsers that do not support canvas.
  ''' </summary>
  <Description("canvas")> Canvas

  ''' <summary>
  ''' HTML caption Tags
  ''' The caption tag defines a table caption.
  ''' The caption tag must be inserted immediately after the table tag.
  ''' Tip: By default, a table caption will be center-aligned above a table. However, the CSS properties text-align and caption-side can be used to align and place the caption.
  ''' </summary>
  <Description("caption")> Caption

  ''' <summary>
  ''' HTML cite Tags
  ''' The cite tag defines the title of a creative work (e.g. a book, a poem, a song, a movie, a painting, a sculpture, etc.).
  ''' Note: A person's name is not the title of a work.
  ''' The text in the cite element usually renders in italic.
  ''' </summary>
  <Description("cite")> Cite

  ''' <summary>
  ''' HTML col Tags
  ''' The col tag specifies column properties for each column within a colgroup element.
  ''' The col tag is useful for applying styles to entire columns, instead of repeating the styles for each cell, for each row.
  ''' </summary>
  <Description("col")> Col

  ''' <summary>
  ''' HTML colgroup Tags
  ''' The colgroup tag specifies a group of one or more columns in a table for formatting.
  ''' The colgroup tag is useful for applying styles to entire columns, instead of repeating the styles for each cell, for each row.
  ''' Note: The colgroup tag must be a child of a table element, after any caption elements and before any thead, tbody, tfoot, and tr elements.
  ''' Tip: To define different properties to a column within a colgroup, use the col tag within the colgroup tag.
  ''' </summary>
  <Description("colgroup")> Colgroup

  ''' <summary>
  ''' HTML data Tags
  ''' The data tag is used to add a machine-readable translation of a given content.
  ''' This element provides both a machine-readable value for data processors, and a human-readable value for rendering in a browser.
  ''' Tip: If the content is time- or date-related, use the time element instead.
  ''' </summary>
  <Description("data")> Data

  ''' <summary>
  ''' HTML datalist Tags
  ''' The datalist tag specifies a list of pre-defined options for an input element.
  ''' The datalist tag is used to provide an "autocomplete" feature for input elements. Users will see a drop-down list of pre-defined options as they input data.
  ''' The datalist element's id attribute must be equal to the input element's list attribute (this binds them together).
  ''' </summary>
  <Description("datalist")> Datalist

  ''' <summary>
  ''' HTML dd Tags
  ''' The dd tag is used to describe a term/name in a description list.
  ''' The dd tag is used in conjunction with dl (defines a description list) and dt (defines terms/names).
  ''' Inside a dd tag you can put paragraphs, line breaks, images, links, lists, etc.
  ''' </summary>
  <Description("dd")> Describe

  ''' <summary>
  ''' HTML dl Tags
  ''' The dl tag defines a description list.
  ''' The dl tag is used in conjunction with dt (defines terms/names) and dd (describes each term/name).
  ''' </summary>
  <Description("dl")> DescriptionList

  ''' <summary>
  ''' HTML dt Tags
  ''' The dt tag defines a term/name in a description list.
  ''' The dt tag is used in conjunction with dl (defines a description list) and dd (describes each term/name).
  ''' </summary>
  <Description("dt")> DescriptionTerm

  ''' <summary>
  ''' HTML del Tags
  ''' The del tag defines text that has been deleted from a document. Browsers will usually strike a line through deleted text.nd dt (defines terms/names).
  ''' </summary>
  <Description("del")> Deleted

  ''' <summary>
  ''' HTML details Tags
  ''' The details tag specifies additional details that the user can open and close on demand.
  ''' The details tag is often used to create an interactive widget that the user can open and close. By default, the widget is closed. When open, it expands, and displays the content within.
  ''' Any sort of content can be put inside the details tag. 
  ''' Tip: The summary tag is used in conjuction with details to specify a visible heading for the details.
  ''' </summary>
  <Description("details")> Details

  ''' <summary>
  ''' HTML dfn Tags
  ''' The dfn tag stands for the "definition element", and it specifies a term that is going to be defined within the content.
  ''' The nearest parent of the dfn tag must also contain the definition/explanation for the term.
  ''' </summary>
  <Description("dfn")> Definition

  ''' <summary>
  ''' HTML dialog Tags
  ''' The dialog tag defines a dialog box or subwindow.
  ''' The dialog element makes it easy to create popup dialogs and modals on a web page.
  ''' </summary>
  <Description("dialog")> Dialog

  ''' <summary>
  ''' HTML div Tags
  ''' The div tag defines a division or a section in an HTML document.
  ''' The div tag is used as a container for HTML elements - which is then styled with CSS or manipulated with JavaScript.
  ''' The div tag is easily styled by using the class or id attribute.
  ''' Any sort of content can be put inside the div tag!
  ''' Note: By default, browsers always place a line break before and after the div element.
  ''' </summary>
  <Description("div")> Div

  ''' <summary>
  ''' HTML em Tags
  ''' The em tag is used to define emphasized text. The content inside is typically displayed in italic.
  ''' A screen reader will pronounce the words in em with an emphasis, using verbal stress.
  ''' </summary>
  <Description("em")> Emphasized

  ''' <summary>
  ''' HTML fieldset Tags
  ''' The fieldset tag is used to group related elements in a form.
  ''' The fieldset tag draws a box around the related elements.
  ''' </summary>
  <Description("fieldset")> Fieldset

  ''' <summary>
  ''' HTML figcaption Tags
  ''' The figcaption tag defines a caption for a figure element.
  ''' The figcaption element can be placed as the first or last child of the figure element.
  ''' </summary>
  <Description("figcaption")> FigCaption

  ''' <summary>
  ''' HTML figure Tags
  ''' The figure tag specifies self-contained content, like illustrations, diagrams, photos, code listings, etc.
  ''' While the content of the figure element is related to the main flow, its position is independent of the main flow, and if removed it should not affect the flow of the document.
  ''' Tip: The figcaption element is used to add a caption for the figure element.
  ''' </summary>
  <Description("figure")> Figure

  ''' <summary>
  ''' HTML footer  Tags
  ''' The footer tag defines a footer for a document or section.
  ''' You can have several footer elements in one document.
  ''' </summary>
  <Description("footer")> Footer

  ''' <summary>
  ''' HTML form Tags
  ''' The form tag is used to create an HTML form for user input.
  ''' </summary>
  <Description("form")> Form


  ''' <summary>
  ''' HTML h1 Tags
  ''' The h1 tags are used to define HTML headings.
  ''' </summary>
  <Description("h1")> H1

  ''' <summary>
  ''' HTML h2 Tags
  ''' The h2 tags are used to define HTML headings.
  ''' </summary>
  <Description("h2")> H2

  ''' <summary>
  ''' HTML h3 Tags
  ''' The h3 tags are used to define HTML headings.
  ''' </summary>
  <Description("h3")> H3

  ''' <summary>
  ''' HTML h4 Tags
  ''' The h4 tags are used to define HTML headings.
  ''' </summary>
  <Description("h4")> H4

  ''' <summary>
  ''' HTML h5 Tags
  ''' The h5 tags are used to define HTML headings.
  ''' </summary>
  <Description("h5")> H5

  ''' <summary>
  ''' HTML h6 Tags
  ''' The h6 tags are used to define HTML headings.
  ''' </summary>
  <Description("h6")> H6

  ''' <summary>
  ''' HTML header Tags
  ''' The header element represents a container for introductory content or a set of navigational links.
  ''' </summary>
  <Description("header")> Header

  ''' <summary>
  ''' HTML hr Tags
  ''' The hr tag defines a thematic break in an HTML page.
  ''' The hr element is most often displayed as a horizontal rule that is used to separate content (or define a change) in an HTML page.
  ''' </summary>
  <Description("hr")> HorizontalRule

  ''' <summary>
  ''' HTML i Tag.
  ''' The i tag defines a part of text in an alternate voice or mood. The content inside is typically displayed in italic.
  ''' The i tag is often used to indicate a technical term, a phrase from another language, a thought, a ship name, etc.
  ''' </summary>
  <Description("i")> I

  ''' <summary>
  ''' HTML iframe Tag.
  ''' The iframe tag specifies an inline frame.
  ''' An inline frame is used to embed another document within the current HTML document.
  ''' Tip: Use CSS to style the iframe.
  ''' Tip: It is a good practice to always include a title attribute for the iframe. This is used by screen readers to read out what the content of the iframe is.
  ''' </summary>
  <Description("iframe")> IFrame

  ''' <summary>
  ''' HTML input Tag.
  ''' The img tag is used to embed an image in an HTML page.
  ''' Images are not technically inserted into a web page; images are linked to web pages. The img tag creates a holding space for the referenced image.
  ''' The img tag has two required attributes:
  ''' src - Specifies the path to the image.
  ''' alt - Specifies an alternate text for the image, if the image for some reason cannot be displayed
  ''' Note: Also, always specify the width and height of an image. If width and height are not specified, the page might flicker while the image loads.
  ''' Tip: To link an image to another document, simply nest the img tag inside an a tag.
  ''' </summary>
  <Description("img")> Image

  ''' <summary>
  ''' HTML input Tag.
  ''' The input tag specifies an input field where the user can enter data.
  ''' The input element is the most important form element.
  ''' The input element can be displayed in several ways, depending on the type attribute.
  ''' </summary>
  <Description("input")> Input

  ''' <summary>
  ''' HTML ins Tag.
  ''' The ins tag in HTML is used to specify a block of inserted text. The ins tag is typically used to mark a range of text that has been added to the document. The inserted text is rendered as underlined text by the web browsers although this property can be changed using CSS text-decoration property. The ins tag requires a starting and ending tag.
  ''' </summary>
  <Description("ins")> Ins

  ''' <summary>
  ''' HTML Label Tag.
  ''' The label tag defines a label for several elements
  ''' </summary>
  <Description("label")> Label

  ''' <summary>
  ''' HTML legend Tag.
  ''' The legend tag defines a caption for the fieldset element.
  ''' </summary>
  <Description("legend")> Legend

  ''' <summary>
  ''' HTML li Tag.
  ''' The li tag defines a list item.
  ''' The li tag is used inside ordered lists(ol), unordered lists (ul), and in menu lists (menu).
  ''' In ul and menu, the list items will usually be displayed with bullet points.
  ''' In ol, the list items will usually be displayed with numbers or letters.
  ''' Tip: Use CSS to style lists.
  ''' </summary>
  <Description("li")> ListItem

  ''' <summary>
  ''' HTML link Tag.
  ''' The link tag defines the relationship between the current document and an external resource.
  ''' The link tag is most often used to link to external style sheets.
  ''' The link element is an empty element, it contains attributes only.
  ''' </summary>
  <Description("link")> Link

  ''' <summary>
  ''' HTML main Tag.
  ''' The main tag specifies the main content of a document.
  ''' The content inside the main element should be unique to the document. It should not contain any content that is repeated across documents such as sidebars, navigation links, copyright information, site logos, and search forms.
  ''' Note: There must not be more than one main element in a document. The main element must NOT be a descendant of an article, aside, footer, header, or nav element.
  ''' </summary>
  <Description("main")> Main

  ''' <summary>
  ''' HTML map Tag.
  ''' The map tag is used to define an image map. An image map is an image with clickable areas.
  ''' The required name attribute of the map element is associated with the img's usemap attribute and creates a relationship between the image and the map.
  ''' The map element contains a number of area elements, that defines the clickable areas in the image map.
  ''' </summary>
  <Description("map")> Map

  ''' <summary>
  ''' HTML mark Tag.
  ''' The mark tag defines text that should be marked or highlighted.
  ''' </summary>
  <Description("mark")> Mark

  ''' <summary>
  ''' HTML meter Tag.
  ''' The meter tag defines a scalar measurement within a known range, or a fractional value. This is also known as a gauge.
  ''' Examples: Disk usage, the relevance of a query result, etc.
  ''' Note: The meter tag should not be used to indicate progress (as in a progress bar). For progress bars, use the progress tag.
  ''' Tip: Always add the label tag for best accessibility practices!
  ''' </summary>
  <Description("meter")> Meter

  ''' <summary>
  ''' HTML nav Tag.
  ''' The nav tag defines a set of navigation links.
  ''' Notice that NOT all links of a document should be inside a nav element. The nav element is intended only for major block of navigation links.
  ''' Browsers, such as screen readers for disabled users, can use this element to determine whether to omit the initial rendering of this content.
  ''' </summary>
  <Description("nav")> Nav

  ''' <summary>
  ''' HTML noscript Tag.
  ''' The noscript tag defines an alternate content to be displayed to users that have disabled scripts in their browser or have a browser that doesn't support s
  ''' The noscript element can be used in both head and body. When used inside head, the noscript element could only contain link, style, and meta elements.
  ''' </summary>
  <Description("noscript")> Noscript

  ''' <summary>
  ''' HTML ol Tag.
  ''' The ol tag defines an ordered list. An ordered list can be numerical or alphabetical.
  ''' The li tag is used to define each list item.
  ''' Tip: Use CSS to style lists.
  ''' Tip: For unordered list, use the ul tag. 
  ''' </summary>
  <Description("ol")> OrderedLists

  ''' <summary>
  ''' HTML optgroup Tag.
  ''' The optgroup tag is used to group related options in a select element (drop-down list).
  ''' If you have a long list of options, groups of related options are easier to handle for a user.
  ''' </summary>
  <Description("optgroup")> Optgroup

  ''' <summary>
  ''' HTML option Tag.
  ''' The option tag defines an option in a select list.
  ''' option elements go inside a select, optgroup, or datalist element.
  ''' Note: The option tag can be used without any attributes, but you usually need the value attribute, which indicates what is sent to the server on form submission.
  ''' Tip: If you have a long list of options, you can group related options within the optgroup tag. 
  ''' </summary>
  <Description("option")> Options

  ''' <summary>
  ''' HTML output Tag.
  ''' The output tag is used to represent the result of a calculation (like one performed by a script).
  ''' </summary>
  <Description("output")> Output

  ''' <summary>
  ''' HTML p Tag.
  ''' The p tag defines a paragraph.
  ''' Browsers automatically add a single blank line before and after each p element.
  ''' </summary>
  <Description("p")> Paragraph

  ''' <summary>
  ''' HTML picture Tag.
  ''' The picture tag gives web developers more flexibility in specifying image resources.
  ''' The most common use of the picture element will be for art direction in responsive designs. Instead of having one image that is scaled up or down based on the viewport width, multiple images can be designed to more nicely fill the browser viewport.
  ''' The picture element contains two tags: one or more source tags and one img tag.
  ''' The browser will look for the first source element where the media query matches the current viewport width, and then it will display the proper image (specified in the srcset attribute). The img element is required as the last child of the picture element, as a fallback option if none of the source tags matches.
  ''' Tip: The picture element works "similar" to video and audio. You set up different sources, and the first source that fits the preferences is the one being used.
  ''' </summary>
  <Description("picture")> Picture

  ''' <summary>
  ''' HTML pre Tag.
  ''' The pre tag defines preformatted text.
  ''' Text in a pre element is displayed in a fixed-width font, and the text preserves both spaces and line breaks. The text will be displayed exactly as written in the HTML source code.
  ''' </summary>
  <Description("pre")> Preformatted

  ''' <summary>
  ''' HTML progress Tag.
  ''' The progress tag represents the completion progress of a task.
  ''' Tip: Always add the label tag for best accessibility practices!
  ''' </summary>
  <Description("progress")> Progress

  ''' <summary>
  ''' HTML q Tag.
  ''' The q tag defines a short quotation.
  ''' Browsers normally insert quotation marks around the quotation.
  ''' Tip: Use blockquote for long quotations. 
  ''' </summary>
  <Description("q")> Quotation

  ''' <summary>
  ''' HTML s Tag.
  ''' The s tag specifies text that is no longer correct, accurate or relevant. The text will be displayed with a line through it.
  ''' The s tag should not be used to define deleted text in a document, use the del tag for that
  ''' </summary>
  <Description("s")> Strikethrough

  ''' <summary>
  ''' HTML section Tag.
  ''' The section tag defines a section in a document.
  ''' </summary>
  <Description("section")> Section

  ''' <summary>
  ''' HTML select Tag.
  ''' The select element is used to create a drop-down list.
  ''' The select element is most often used in a form, to collect user input.
  ''' The name attribute is needed to reference the form data after the form is submitted (if you omit the name attribute, no data from the drop-down list will be submitted).
  ''' The id attribute is needed to associate the drop-down list with a label.
  ''' The option tags inside the select element define the available options in the drop-down list.
  ''' Tip: Always add the label tag for best accessibility practices!
  ''' </summary>
  <Description("select")> Selection

  ''' <summary>
  ''' HTML small Tag.
  ''' The small tag defines smaller text (like copyright and other side-comments).
  ''' Tip: This tag is not deprecated, but it is possible to achieve richer (or the same) effect with CSS.
  ''' </summary>
  <Description("small")> Small

  ''' <summary>
  ''' HTML source Tag.
  ''' The source tag is used to specify multiple media resources for media elements, such as video, audio, and picture.
  ''' The source tag allows you to specify alternative video/audio/image files which the browser may choose from, based on browser support or viewport width. The browser will choose the first source it supports.
  ''' </summary>
  <Description("source")> Source

  ''' <summary>
  ''' HTML span Tag.
  ''' The span tag is an inline container used to mark up a part of a text, or a part of a document.
  ''' The span tag is easily styled by CSS or manipulated with JavaScript using the class or id attribute.
  ''' The span tag is much like the div element, but div is a block-level element and span is an inline element.
  ''' </summary>
  <Description("span")> Span

  ''' <summary>
  ''' HTML Strong Tag.
  ''' The strong tag is used to define text with strong importance. The content inside is typically displayed in bold.
  ''' Tip: Use the b tag to specify bold text without any extra importance!
  ''' </summary>
  <Description("strong")> Strong

  ''' <summary>
  ''' HTML style Tag.
  ''' The style tag is used to define style information (CSS) for a document.
  ''' Inside the style element you specify how HTML elements should render in a browser.
  ''' </summary>
  <Description("style")> Style

  ''' <summary>
  ''' HTML sub Tag.
  ''' The sub tag defines subscript text. Subscript text appears half a character below the normal line, and is sometimes rendered in a smaller font. Subscript text can be used for chemical formulas, like H2O.
  ''' Tip: Use the sup tag to define superscripted text.
  ''' </summary>
  <Description("sub")> Subscript

  ''' <summary>
  ''' HTML summary Tag.
  ''' The summary tag defines a visible heading for the details element. The heading can be clicked to view/hide the details.
  ''' Note: The summary element should be the first child element of the details element.
  ''' </summary>
  <Description("summary")> Summary

  ''' <summary>
  ''' HTML sup Tag.
  ''' The sup tag defines superscript text. Superscript text appears half a character above the normal line, and is sometimes rendered in a smaller font. Superscript text can be used for footnotes, like WWW[1].
  ''' Tip: Use the sub tag to define subscript text.
  ''' </summary>
  <Description("sup")> Superscript

  ''' <summary>
  ''' HTML svg Tag.
  ''' The svg tag defines a container for SVG graphics.
  ''' SVG has several methods for drawing paths, boxes, circles, text, and graphic images
  ''' </summary>
  <Description("svg")> SVG

  ''' <summary>
  ''' HTML table Tag.
  ''' An HTML table consists of one table element and one or more tr, th, and td elements.
  ''' SVG has several methods for drawing paths, boxes, circles, text, and graphic images
  ''' </summary>
  <Description("table")> Table

  ''' <summary>
  ''' HTML tbody Tag.
  ''' The tbody tag is used to group the body content in an HTML table.
  ''' The tbody element is used in conjunction with the thead and tfoot elements to specify each part of a table (body, header, footer).
  ''' Browsers can use these elements to enable scrolling of the table body independently of the header and footer. Also, when printing a large table that spans multiple pages, these elements can enable the table header and footer to be printed at the top and bottom of each page.
  ''' Note: The tbody element must have one or more tr tags inside.
  ''' The tbody tag must be used in the following context: As a child of a table element, after any caption, colgroup, and thead elements.
  ''' Tip: The thead, tbody, and tfoot elements will not affect the layout of the table by default. However, you can use CSS to style these elements!
  ''' </summary>
  <Description("tbody")> Tbody

  ''' <summary>
  ''' HTML td Tag.
  ''' The td tag defines a standard data cell in an HTML table.
  ''' An HTML table has two kinds of cells: Header cells (th), Data cells (td)
  ''' The text in td elements are regular and left-aligned by default.
  ''' The text in th elements are bold and centered by default. 
  ''' </summary>
  <Description("td")> TableData

  ''' <summary>
  ''' HTML tfoot Tag.
  ''' The tfoot tag is used to group footer content in an HTML table.
  ''' The tfoot element is used in conjunction with the thead and tbody elements to specify each part of a table (footer, header, body).
  ''' Browsers can use these elements to enable scrolling of the table body independently of the header and footer. Also, when printing a large table that spans multiple pages, these elements can enable the table header and footer to be printed at the top and bottom of each page.
  ''' Note: The tfoot element must have one or more tr tags inside.
  ''' The tfoot tag must be used in the following context: As a child of a table element, after any caption, colgroup, thead, and tbody elements.
  ''' Tip: The thead, tbody, and tfoot elements will not affect the layout of the table by default. However, you can use CSS to style these elements!
  ''' </summary>
  <Description("tfoot")> TableFooter

  ''' <summary>
  ''' HTML th Tag.
  ''' The th tag defines a header cell in an HTML table.
  ''' An HTML table has two kinds of cells: Header cells (th), Data cells (td)
  ''' The text in th elements are bold and centered by default. 
  ''' The text in td elements are regular and left-aligned by default.
  ''' </summary>
  <Description("th")> TableHeader

  ''' <summary>
  ''' HTML thead Tag.
  ''' The thead tag is used to group header content in an HTML table.
  ''' The thead element is used in conjunction with the tbody and tfoot elements to specify each part of a table (header, body, footer).
  ''' Browsers can use these elements to enable scrolling of the table body independently of the header and footer. Also, when printing a large table that spans multiple pages, these elements can enable the table header and footer to be printed at the top and bottom of each page.
  ''' Note: The thead element must have one or more tr tags inside.
  ''' The thead tag must be used in the following context: As a child of a table element, after any caption and colgroup elements, and before any tbody, tfoot, and tr elements.
  ''' Tip: The thead, tbody, and tfoot elements will not affect the layout of the table by default. However, you can use CSS to style these elements!
  ''' </summary>
  <Description("thead")> Thead

  ''' <summary>
  ''' HTML tr Tag.
  ''' The tr tag defines a row in an HTML table.
  ''' A tr element contains one or more th or td elements.
  ''' </summary>
  <Description("tr")> Tablerow

  ''' <summary>
  ''' HTML template Tag.
  ''' The template tag is used as a container to hold some HTML content hidden from the user when the page loads.
  ''' The content inside template can be rendered later with a JavaScript.
  ''' You can use the template tag if you have some HTML code you want to use over and over again, but not until you ask for it. To do this without the template tag, you have to create the HTML code with JavaScript to prevent the browser from rendering the code.
  ''' </summary>
  <Description("template")> Template

  ''' <summary>
  ''' HTML textarea Tag.
  ''' The textarea tag defines a multi-line text input control.
  ''' The textarea element is often used in a form, to collect user inputs like comments or reviews.
  ''' A text area can hold an unlimited number of characters, and the text renders in a fixed-width font (usually Courier).
  ''' The size of a text area is specified by the cols and rows attributes (or with CSS).
  ''' The name attribute is needed to reference the form data after the form is submitted (if you omit the name attribute, no data from the text area will be submitted).
  ''' The id attribute is needed to associate the text area with a label.
  ''' Tip: Always add the label tag for best accessibility practices!
  ''' </summary>
  <Description("textarea")> Textarea

  ''' <summary>
  ''' HTML textarea Tag.
  ''' The time tag defines a specific time (or datetime).
  ''' The datetime attribute of this element is used translate the time into a machine-readable format so that browsers can offer to add date reminders through the user's calendar, and search engines can produce smarter search results.
  ''' </summary>
  <Description("time")> Time

  ''' <summary>
  ''' HTML track Tag.
  ''' The track tag specifies text tracks for audio or video elements.
  ''' This element is used to specify subtitles, caption files or other files containing text, that should be visible when the media is playing.
  ''' Tracks are formatted in WebVTT format (.vtt files).
  ''' </summary>
  <Description("track")> Track

  ''' <summary>
  ''' HTML ul Tag.
  ''' The ul tag defines an unordered (bulleted) list.
  ''' Use the ul tag together with the li tag to create unordered lists.
  ''' Tip: Use CSS to style lists.
  ''' Tip: For ordered lists, use the ol tag. 
  ''' </summary>
  <Description("ul")> UnorderedList

  ''' <summary>
  ''' HTML var Tag.
  ''' The var tag is used to defines a variable in programming or in a mathematical expression. The content inside is typically displayed in italic.
  ''' Tip: This tag is not deprecated. However, it is possible to achieve richer effect by using CSS.
  ''' </summary>
  <Description("var")> Variable

  ''' <summary>
  ''' HTML video Tag.
  ''' The video tag is used to embed video content in a document, such as a movie clip or other video streams.
  ''' The video tag contains one or more source tags with different video sources. The browser will choose the first source it supports.
  ''' The text between the video opening and closing tags will only be displayed in browsers that do not support the video element.
  ''' There are three supported video formats in HTML: MP4, WebM, and OGG.
  ''' </summary>
  <Description("video")> Video

  ''' <summary>
  ''' HTML wbr Tag.
  ''' The wbr (Word Break Opportunity) tag specifies where in a text it would be ok to add a line-break.
  ''' Tip: When a word is too long, the browser might break it at the wrong place. You can use the wbr element to add word break opportunities.
  ''' </summary>
  <Description("wbr")> WordBreakOpportunity
End Enum
