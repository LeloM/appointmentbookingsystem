﻿Imports ABSLib

Namespace Appointment_Booking_System
  Public Class ABSPageBase(Of VM As Singular.Web.IViewModel)
    Inherits Singular.Web.PageBase(Of VM)

    Protected Overrides Sub OnInit(ByVal e As EventArgs)
      MyBase.OnInit(e)

      If ABSLib.ABSLib.Security.ABSWebSecurity.HasAuthenticatedUser() Then
        If ABSLib.ABSLib.Security.ABSWebSecurity.CurrentIdentity().ResetStates =
            ABSLib.ABSLib.Security.ResetState.MustResetPassword Then
          Singular.Web.Misc.NavigationHelper.RedirectAndRemember("~/Account/ChangePassword.aspx?WasReset = True")
        End If
      End If
    End Sub
  End Class
End Namespace
