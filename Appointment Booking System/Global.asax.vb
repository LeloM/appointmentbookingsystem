﻿Imports System
Imports Singular.Web
Imports ABSLib.ABSLib
Imports ABSLib.ABSLib.Security

Namespace Appointment_Booking_System
  Public Class [Global]
    Inherits ApplicationSettings(Of ABSIdentity)

    Protected Overrides Sub ApplicationSetup()
      WebError.SupportsWebError = True
      'CommonData.DefaultLifeTime = New TimeSpan(1, 0, 0)
      'CommonData.GetCachedLists()
      Singular.Web.Controls.DefaultButtonStyle = Singular.Web.ButtonStyle.Bootstrap
      Singular.Web.Controls.Controls.DefaultButtonPostBackType = Singular.Web.PostBackType.Ajax
      Singular.Web.Controls.Controls.DefaultDropDownType = Singular.DataAnnotations.DropDownWeb.SelectType.Combo
      Singular.Web.Controls.Controls.UsesPagedGrid = True
      Singular.Documents.Settings.PassUserIDToGetProc = True
      Singular.Documents.Settings.DocumentHashesEnabled = True
      Singular.Emails.EMailBuilder.RegardsText = "Regards<br/>Appointment Booking System"
      'Singular.SystemSettings.General.RegisterSettingsClass(Of ABSLib.ABSLib.CorrespondenceSettings) ()
      Singular.Web.Scripts.Scripts.Settings.LibJQueryVersion = Singular.Web.Scripts.ScriptSettings.JQueryVersion.JQ_1_12_4
      Singular.Web.Scripts.Scripts.Settings.LibJQueryUIVersion = Singular.Web.Scripts.ScriptSettings.JQueryUIVersion.JQ_UI_1_12_1
    End Sub

    Public Overrides Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
      MyBase.Session_Start(sender, e)
      'CommonData.InitialiseSessionLists()
    End Sub
  End Class
End Namespace
