﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web

Namespace Appointment_Booking_System.CustomControls
  Public Class MainMenu
    Inherits System.Web.UI.WebControls.WebControl

    Public Property SiteMapDataSourceID As String

    Protected Overrides Sub Render(ByVal writer As System.Web.UI.HtmlTextWriter)
      Dim Script As String = ""
      Script += " var Links = document.getElementsByClassName(""SingularLink"");"
      Script += " for (i = 0; i < Links.length; i++) {"
      Script += " if (Links[i].href.toString() == window.location.toString()) {"
      Script += " Links[i].className = ""SelectedItem"";"
      Script += " }"
      Script += " }"
      Page.ClientScript.RegisterStartupScript(Me.[GetType](), "ManagementMenu", Script, True)
      Dim DS As System.Web.UI.WebControls.SiteMapDataSource = CType(FindControl(SiteMapDataSourceID), System.Web.UI.WebControls.SiteMapDataSource)
      Dim RootNode As SiteMapNode = DS.Provider.RootNode
      writer.WriteLine("<div class='sidebar-nav nav nav-stacked' id='sidebar'>")
      writer.WriteLine("<div id='MainMenu'>")

      For Each node As SiteMapNode In RootNode.ChildNodes

        If node.Roles.Count = 0 Then
          writer.Write(GetNodeString(node))
        ElseIf Singular.Security.Security.HasAccess(node.Roles(0).ToString()) Then
          writer.Write(GetNodeString(node))
        End If
      Next

      writer.WriteLine("</div>")
      writer.WriteLine("</div>")
    End Sub

    Private Function GetNodeString(ByVal node As SiteMapNode) As String
      Dim SB As System.Text.StringBuilder = New System.Text.StringBuilder()

      If node.HasChildNodes Then
        SB.Append("<a ")
        SB.Append("id='" & node("id") & "' ")

        If Not (String.IsNullOrWhiteSpace(node("onclick"))) Then
          SB.Append("onclick='" & node("onclick") & "' ")
        End If

        If Not (String.IsNullOrWhiteSpace(node("class"))) Then
          SB.Append("class='" & node("class") & "' ")
        End If

        SB.Append("data-toggle = 'collapse'")
        SB.Append("style = 'height:auto'")
        SB.Append("data-parent='#MainMenu' ")
      Else

        If (node.Title.Contains("Menu-Close")) Then
          SB.Append("<a href='#' ")
          SB.Append("id='menu-toggle' ")
          SB.Append("onclick='doTheToggle()' ")
        Else
          SB.Append("<a href='" & node.Url & "' ")
        End If

        SB.Append("class='list-group-item' ")
        SB.Append("data-parent='#MainMenu' ")
      End If

      SB.Append("id='" & node("id") & "' > ")
      SB.Append("<em>" & node.Title & "</em>")

      If (node.Title.Contains("Menu")) Then
        SB.Append("<span id ='main_icon' class='" & node("glyphicon") & "' >")
      Else
        SB.Append("<span class='" & node("glyphicon") & "' >")
      End If

      SB.Append("</span>")

      If node.HasChildNodes Then
        SB.Append("<i class='fa fa-caret-down'></i>")
        SB.Append("</a>")
        SB.Append("<div class='submenu panel-collapse collapse' id='" & node.Title.ToLower() & "'>")
      Else
        SB.Append("</a>")
      End If

      If node.HasChildNodes Then

        For Each SubNode As SiteMapNode In node.ChildNodes

          If SubNode.Roles.Count = 0 Then
            SB.Append(GetNodeStringChild(SubNode))
          ElseIf Singular.Security.Security.HasAccess(SubNode.Roles(0).ToString()) Then
            SB.Append(GetNodeStringChild(SubNode))
          End If
        Next
      End If

      If node.HasChildNodes Then
        SB.Append("</div>")
      End If

      Return SB.ToString()
    End Function

    Private Function GetNodeStringChild(ByVal node As SiteMapNode) As String
      Dim SB As System.Text.StringBuilder = New System.Text.StringBuilder()

      If node.HasChildNodes Then
        SB.Append("<a")
        SB.Append(" onclick='")
        SB.Append(" if (!this.parentElement.children) { return }")
        SB.Append(" if (this.parentElement.children.length <= 1) { return }")
        SB.Append(" if (this.parentElement.children[1].clientHeight > 0) {")
        SB.Append("  this.parentElement.children[1].style.display = 'none';")
        SB.Append(" } else {")
        SB.Append("  this.parentElement.children[1].style.display = 'block';")
        SB.Append(" };' ")
        SB.Append(" >")
      Else
        SB.Append("<a  href='" & node.Url & "' ")
        SB.Append("id='" & node("id") & "' ")

        If Not (String.IsNullOrWhiteSpace(node("class"))) Then
          SB.Append("class='" & node("class") & "' ")
        End If

        If Not (String.IsNullOrWhiteSpace(node("onclick"))) Then
          SB.Append("onclick='" & node("onclick") & "' ")
        End If

        If Not (String.IsNullOrWhiteSpace(node("data-toggle"))) Then
          SB.Append("data-toggle='" & node("data-toggle") & "' ")
        End If

        If Not (String.IsNullOrWhiteSpace(node("parent")) Or node("parent") = "Client") Then
          SB.Append("data-parent='" & node.Url & "' ")
        End If

        SB.Append(">")
      End If

      SB.Append("<em>")
      SB.Append(node.Title)
      SB.Append("</em>")

      If node("glyphicon") IsNot Nothing Then
        SB.Append("<span class='" & node("glyphicon") & "' ></span>")
      Else

        If node("font-awesome") IsNot Nothing Then
          SB.Append("<span class='" & node("font-awesome") & "' ></span>")
        End If
      End If

      If node.HasChildNodes Then
        SB.Append("<span class='' ></span>")
      End If

      SB.Append("</a>")

      If node.HasChildNodes Then

        For Each SubNode As SiteMapNode In node.ChildNodes

          If SubNode.Roles.Count = 0 Then
            SB.Append(GetNodeStringChild(SubNode))
          ElseIf Singular.Security.Security.HasAccess(SubNode.Roles(0).ToString()) Then
            SB.Append(GetNodeStringChild(SubNode))
          End If
        Next
      End If

      Return SB.ToString()
    End Function

    Protected Overridable Function HasAccess(ByVal PagePath As String) As Boolean
      Return Singular.Web.CustomControls.SiteMapDataSource.HasAccess(PagePath)
    End Function
  End Class
End Namespace

