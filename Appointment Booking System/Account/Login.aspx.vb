﻿Imports System.Web
Imports System.Web.UI
Imports Singular.Web
Imports Singular.Web.Security
Imports Singular.Web.Controls
Imports ABSLib.ABSLib.Security
Imports Appointment_Booking_System.Appointment_Booking_System

Namespace Account
  Public Class Login
    Inherits ABSPageBase(Of LoginVM)
  End Class

  Public Class LoginVM
    Inherits ABSStatelessViewModel(Of LoginVM)

    Public Property LoginDetails As LoginDetails

    ''' <summary>
    ''' The location to direct to after login
    ''' </summary>
    Public Property RedirectLocation As String

    Protected Overrides Sub Setup()
      MyBase.Setup()

      Me.LoginDetails = New LoginDetails()

      Me.ValidationMode = ValidationMode.OnSubmit
      Me.ValidationDisplayMode = ValidationDisplayMode.Controls

      If Page.Request.QueryString("ReturnUrl") = "/Appointment_Booking_System/default.aspx" Then
        Me.RedirectLocation = VirtualPathUtility.ToAbsolute(Security.GetSafeRedirectUrl("~/Account/Home.aspx", "~/Account/Home.aspx"))
      Else
        Me.RedirectLocation = VirtualPathUtility.ToAbsolute(Security.GetSafeRedirectUrl(Page.Request.QueryString("ReturnUrl"), "~/Account/Home.aspx"))
      End If
    End Sub

    <WebCallable(LoggedInOnly:=False)>
    Public Function Login(ByVal loginDetails As LoginDetails) As Result
      Return ABSIdentity.Login(loginDetails)
    End Function

  End Class
End Namespace