﻿<%@ Page Title="Home" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="Home.aspx.vb" Inherits="Appointment_Booking_System.Home" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
  <link href="../Styles/dashboard.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <h2>Welcome to the Appointment Booking System</h2>

  <%If ABSLib.ABSLib.Security.ABSWebSecurity.HasAuthenticatedUser() Then%>
    <div>How can we help you today :-)</div>
  <%End If%>

  <%If Not ABSLib.ABSLib.Security.ABSWebSecurity.HasAuthenticatedUser() Then%>
    <div>Looking forward to interacting with you again!</div>
  <%End If %>
</asp:Content>
