﻿<%@ Page Title="Log In" Language="vb" AutoEventWireup="false" MasterPageFile="~/SiteLoggedOut.Master" CodeBehind="Login.aspx.vb" Inherits="Appointment_Booking_System.Account.Login" %>
<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
  <%Using h = Helpers
      h.MessageHolder()

      Dim LoginDiv = h.Div()
      Dim ImgDiv1 = LoginDiv.Helpers.DivC("row text-center")
      Dim ImgDiv = ImgDiv1.Helpers.HTML("<img class='LoginImgSize' src='.../Images/OFFICEEXPRESSLOGO.fw.png' />")
      Dim fieldSet = LoginDiv.Helpers.FieldSetFor(Of Singular.Web.Security.LoginDetails)("Account Information", Function(c) c.LoginDetails)
      fieldSet.AddClass("StackedEditors SUI-RuleBorder")

      Dim row2 = fieldSet.Helpers.DivC("TextCenter")
      row2.Helpers.LabelFor(Function(c) c.UserName)
      row2.Helpers.EditorFor(Function(c) c.UserName).AddClass("WidthInput")

      Dim row = fieldSet.Helpers.DivC("TextCenter")
      row.Helpers.LabelFor(Function(c) c.Password)
      row.Helpers.EditorFor(Function(c) c.Password).AddClass("WidthInput")

      Dim RememberDiv = fieldSet.Helpers.DivC("TextCentre")
      Dim row3 = RememberDiv.Helpers.DivC("row")
      row3.Helpers.EditorFor(Function(c) c.RememberMe)
      row3.Helpers.LabelFor(Function(c) c.RememberMe).AddClass("MarginRight5")

      Dim ButtonDiv = fieldSet.Helpers.DivC("TextCenter")
      Dim button = ButtonDiv.Helpers.Button("Login", "Log In")
      button.AddClass("")
      button.AddBinding(Singular.Web.KnockoutBindingString.click, "Login()")
      button.ClickOnEnterKey = True
      button.Image.Glyph = Singular.Web.FontAwesomeIcon.lock
    End Using
    %>

  <script type="text/javascript">
    function Login() {
      Singular.Validation.IfValid(ViewModel, function () {
        ViewModel.CallServerMethod('Login', { LoginDetails: ViewModel.LoginDetails.Serialise() }, function (result) {
          if (result.Success) {
            window.location = result.Data ? result.Data : ViewModel.RedirectLocation();
          } else {
            ViewModel.LoginDetails().Password('');
            Singular.AddMessage(1, 'Login', result.ErrorText).Fade(2000);
          }
        });
      });
    }
  </script>
</asp:Content>
