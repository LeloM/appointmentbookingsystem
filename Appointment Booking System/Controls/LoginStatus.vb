﻿Imports System.Web
Imports System.Web.Security
Imports Singular.Web
Imports Singular.Web.Controls
Imports ABSLib.ABSLib.Security
Imports Singular.Web.CustomControls

Namespace Appointment_Booking_System.Controls
  Public Class LoginStatus
    Inherits HelperControls.HelperBase(Of Object)

    Protected Overrides Sub Setup()
      MyBase.Setup()
      Dim container As HTMLDiv(Of Object) = Helpers.Div()
      container.Style.Display = Display.inlineblock
      container.Style.MarginLeft("20px")

      Dim loginUrl As String =
        VirtualPathUtility.ToAbsolute(FormsAuthentication.LoginUrl)

      Dim defaultUrl As String =
        VirtualPathUtility.ToAbsolute(FormsAuthentication.DefaultUrl)

      If Singular.Security.Security.HasAuthenticatedUser Then
        Dim identity As ABSIdentity = ABSWebSecurity.CurrentIdentity()
        Dim loginStatus = container.Helpers.DivC("login-status")

        If True Then
          loginStatus.Attributes("data-ContextMenu") = "cmSecurity"
          Dim userLabel = loginStatus.Helpers.HTMLTag("span", "Hello " & "identity.FirstNames")

          If True Then
            userLabel.Style.Display = Display.inlineblock
          End If

          loginStatus.Helpers.Image().Glyph =
            Singular.Web.FontAwesomeIcon.user
          Dim contextMenu = loginStatus.Helpers.DivC("context-menu-ls")

          If True Then
            contextMenu.Attributes("id") = "cmSecurity"
            contextMenu.Style.TextAlign = Singular.Web.TextAlign.right
            Dim contextMenuMain = contextMenu.Helpers.DivC("CM-Main")

            If True Then
              Dim contextMenuMainHeader = contextMenuMain.Helpers.Div()

              If True Then
                contextMenuMainHeader.AddClass("CM-Header")
                contextMenuMainHeader.Helpers.Div().Helpers.HTML(identity.UserNameReadable)
                contextMenuMainHeader.Helpers.Div().Helpers.HTML(identity.EmailAddresss)
              End If

              Dim contextMenuBody = contextMenuMain.Helpers.Div()

              If True Then
                contextMenuBody.AddClass("Selectable")
                Dim contextMenuChangePassword = contextMenu.Helpers.Div()

                If True Then
                  contextMenuChangePassword.Helpers.LinkFor(Nothing, Nothing, VirtualPathUtility.ToAbsolute("~/Account/ChangePassword.aspx"), "ChangePassword")
                End If
              End If

              Dim contextMenuLogout = contextMenuBody.Helpers.Div()

              If True Then
                contextMenuLogout.Helpers.LinkFor(Nothing, Nothing, defaultUrl & "?Scmd=Logout", "Logout")
              End If
            End If
          End If
        End If
      Else
        Dim loggedOutDiv = container.Helpers.Div()

        If True Then
          loggedOutDiv.Style.FontSize = "14px"
          loggedOutDiv.Helpers.LinkFor(Nothing, Nothing, loginUrl, "Login").Style("text-decoration") = "none"
          loggedOutDiv.Helpers.Image().Glyph = FontAwesomeIcon.user
        End If
      End If

    End Sub

    Protected Overrides Sub Render()
      MyBase.Render()

      RenderChildren()
    End Sub
  End Class

End Namespace