﻿Namespace MaskedFields

  ''' <summary>
  ''' Blur out text and hide data
  ''' </summary>
  <AttributeUsage(AttributeTargets.Property)>
  Public Class MaskedFieldAttribute
    Inherits Attribute

    Public Property AllowedSecurityRoles As String()

    Public Property MaskedType As MaskedType

    ''' <summary>
    ''' Initializes a new instance of the <see cref="MaskedFieldAttribute"/> class.
    ''' </summary>
    ''' <param name="allowedSecurityRoles">Security Rules allowed for value to return and text to not be blurred out</param>
    ''' <param name="maskedType"><see cref="MaskedFields.MaskedType"/> used to mask field</param>
    Public Sub New(allowedSecurityRoles As String(), maskedType As MaskedType)
      Me.AllowedSecurityRoles = allowedSecurityRoles
      Me.MaskedType = maskedType
    End Sub

    Public Function Allowed() As Boolean
      If Singular.Security.HasAuthenticatedUser Then
        'Check that the user has at least 1 role.
        Dim HasRole As Boolean = False
        If AllowedSecurityRoles Is Nothing OrElse AllowedSecurityRoles.Length = 0 Then
          Return True
        Else
          For Each role As String In AllowedSecurityRoles
            If Singular.Security.HasAccess(role) Then
              Return True
            End If
          Next
          Return False
        End If
      Else
        'Not authenticated
        Return False
      End If
    End Function
  End Class

End Namespace
