﻿Namespace MaskedFields
  Public Enum MaskedType
    ''' <summary>
    ''' Blur out text and mask data
    ''' </summary>
    Blur

    ''' <summary>
    ''' Asterisk out text and mask data
    ''' </summary>
    Asterisk
  End Enum
End Namespace
